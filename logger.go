package rabbitmq_manager

import (
	"fmt"
	"log"
	"os"
	"time"
)

type Level int8

const (
	// DebugLevel defines debug log level.
	DebugLevel Level = iota
	// InfoLevel defines info log level.
	InfoLevel
	// WarnLevel defines warn log level.
	WarnLevel
	// ErrorLevel defines error log level.
	ErrorLevel
	// FatalLevel defines fatal log level.
	FatalLevel
	// PanicLevel defines panic log level.
	PanicLevel
	// NoLevel defines an absent log level.
	NoLevel
	// Disabled disables the logger.
	Disabled

	// TraceLevel defines trace log level.
	TraceLevel Level = -1
)

type Logger interface {
	Trace(msg string)
	Tracef(format string, v ...interface{})
	Debug(msg string)
	Debugf(format string, v ...interface{})
	Info(msg string)
	Infof(format string, v ...interface{})
	Warn(msg string)
	Warnf(format string, v ...interface{})
	Error(msg string)
	Errorf(format string, v ...interface{})
	Fatal(msg string)
	Fatalf(format string, v ...interface{})
	Panic(msg string)
	Panicf(format string, v ...interface{})
	Print(msg string)
	Printf(format string, v ...interface{})
	SetLevel(level Level)
}

type StdLogger struct {
	level  Level
	logger *log.Logger
}

func (l *StdLogger) Trace(msg string) {
	l.printWithLevel(TraceLevel, msg)
}

func (l *StdLogger) Tracef(format string, v ...interface{}) {
	l.printfWithLevel(TraceLevel, format, v...)
}

func (l *StdLogger) Debug(msg string) {
	l.printWithLevel(DebugLevel, msg)
}

func (l *StdLogger) Debugf(format string, v ...interface{}) {
	l.printfWithLevel(DebugLevel, format, v...)
}

func (l *StdLogger) Info(msg string) {
	l.printWithLevel(InfoLevel, msg)
}

func (l *StdLogger) Infof(format string, v ...interface{}) {
	l.printfWithLevel(InfoLevel, format, v...)
}

func (l *StdLogger) Warn(msg string) {
	l.printWithLevel(WarnLevel, msg)
}

func (l *StdLogger) Warnf(format string, v ...interface{}) {
	l.printfWithLevel(WarnLevel, format, v...)
}

func (l *StdLogger) Error(msg string) {
	l.printWithLevel(ErrorLevel, msg)
}

func (l *StdLogger) Errorf(format string, v ...interface{}) {
	l.printfWithLevel(ErrorLevel, format, v...)
}

func (l *StdLogger) Fatal(msg string) {
	l.printWithLevel(FatalLevel, msg)
}

func (l *StdLogger) Fatalf(format string, v ...interface{}) {
	l.printfWithLevel(FatalLevel, format, v...)
}

func (l *StdLogger) Panic(msg string) {
	l.printWithLevel(PanicLevel, msg)
}

func (l *StdLogger) Panicf(format string, v ...interface{}) {
	l.printfWithLevel(PanicLevel, format, v...)
}

func (l *StdLogger) Print(msg string) {
	l.printWithLevel(NoLevel, msg)
}

func (l *StdLogger) Printf(format string, v ...interface{}) {
	l.printfWithLevel(NoLevel, format, v...)
}

func (l *StdLogger) SetLevel(level Level) {
	l.level = level
}

func (l *StdLogger) printWithLevel(level Level, msg string) {
	prefix := l.getLevelPrefix(level)
	t := l.getLogTime()
	layout := fmt.Sprintf("%s [%s] %s", t, prefix, msg)
	if l.level > level || l.level == Disabled {
		return
	} else if level == FatalLevel {
		l.logger.Fatalln(layout)
	} else if level == PanicLevel {
		l.logger.Panicln(layout)
	} else {
		l.logger.Println(layout)
	}
}

func (l *StdLogger) printfWithLevel(level Level, format string, v ...interface{}) {
	prefix := l.getLevelPrefix(level)
	t := l.getLogTime()
	layout := fmt.Sprintf("%s [%s] %s", t, prefix, format)
	if l.level > level || l.level == Disabled {
		return
	} else if level == FatalLevel {
		l.logger.Fatalf(layout, v...)
	} else if level == PanicLevel {
		l.logger.Panicf(layout, v...)
	} else {
		l.logger.Printf(layout, v...)
	}
}

func (l *StdLogger) getLevelPrefix(level Level) string {
	switch level {
	case DebugLevel:
		return "DBG"
	case InfoLevel:
		return "INF"
	case TraceLevel:
		return "TRC"
	case WarnLevel:
		return "WRN"
	case ErrorLevel:
		return "ERR"
	case FatalLevel:
		return "FTL"
	case PanicLevel:
		return "PNC"
	case NoLevel:
		return "-"
	default:
		return ""
	}
}

func (l *StdLogger) getLogTime() string {
	now := time.Now().UTC()
	return now.Format("2006-01-02T15:04:05Z")
}

func (l *StdLogger) Start(level Level) {
	l.logger = log.New(os.Stdout, "", 0)
	l.SetLevel(level)
}

var stdLogger *StdLogger

func init() {
	stdLogger = &StdLogger{}
	stdLogger.Start(DebugLevel)
}
