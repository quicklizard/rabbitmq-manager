# RabbitMQ Manager

`rabbitm-manager` is a utility library that wraps `github.com/streadway/amqp` AMQP client and provides the common building blocks to work
against a RabbitMQ broker, without the need to handle lower-level connection, channel and queue management.

** CODE IS CONSIDERED ALPHA - DO NOT USE IN PRODUCTION **

## Installation

To install, run `go get bitbucket.org/quicklizard/rabbitmq-manager`.

`rabbitmq-manager` is compatible with Go 1.12 and Go modules.

## Components

`rabbitmq-manager` provides four main components to work with RabbitMQ:

* Broker - wraps connection management, consumers and publishers for a single RabbitMQ broker / cluster
* Consumer - defines a RabbitMQ consumer that consumes messages from a given queue
* Publisher - defines a RabbitMQ message publisher that publishes messages to a given exchange with routing key
* Conn - defines a TCP connection to a RabbitMQ broker / cluster with reconnect support

Ideally, you should use the `Broker` component as an entry point to work with your RabbitMQ broker / cluster. The `Broker` component provides
connection management, as well as adding consumers and publishers and does all the "heavy lifting" for you.

## Usage

```go
package main

import (
  rmq "bitbucket.org/quicklizard/rabbitmq-manager"
  "fmt"
  "github.com/streadway/amqp"
)

func main() {
  var err error
  broker := rmq.NewBroker("amqp://guest:guest@localhost:5672/")
  handler := func(delivery amqp.Delivery) {
    msg := string(delivery.Body)
    fmt.Println(msg)
    delivery.Ack(false)
  }
  // add a new consumer
  err = broker.Consume("test-queue", "amq.direct", "direct", "test.queue", 1, false, handler)
  if err != nil {
    fmt.Println(err)
    return
  }
  // publish a message
  if err = broker.Publish("amq.direct", "test.queue", "test"); err != nil {
    fmt.Println(err)
  }
  
  // close the broker's connections
  broker.Close()
}
```