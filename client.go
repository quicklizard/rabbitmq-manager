package rabbitmq_manager

import (
	"github.com/streadway/amqp"
	"time"
)

// Client encapsulates the use of AMQP connections
// ensuring consumers and publishers work in separate connections
type Client struct {
	consumerConn  *Connection
	publisherConn *Connection
	url           string
}

// New takes an AMQP connection URL and creates a new Client object
func New(url string) *Client {
	Client := &Client{
		consumerConn:  nil,
		publisherConn: nil,
		url:           url,
	}
	return Client
}

// Consume creates a new consumer on a queue, bound to an exchange via a routingKey, defined with a message
// prefetch count, and a provided Handler function to handle messages from the queue.
//
// Consume returns an error if creating the underlying Connection object resulted in an error, or if
// registering a consumer to the queue resulted in an error.
func (b *Client) Consume(queue, exchange, exchangeType, routingKey string, prefetch int, handler Handler) error {
	var err error
	var conn *Connection
	if b.consumerConn == nil {
		if conn, err = NewConnection(b.url); err != nil {
			return err
		}
		b.consumerConn = conn
	}
	return b.consumerConn.Consume(queue, exchange, exchangeType, routingKey, prefetch, handler)
}

// ConsumeOnce will consume a single message from the provided queue, bound to an exchange via a routingKey.
//
// Under the hood, ConsumeOnce will create a disposable AMQP channel, and use it to get a single message from the queue.
// Every call to ConsumeOnce creates a new AMQP channel, which may create a performance overhead if used repeatedly.
//
// ConsumeOnce will return an error if creating the underlying Connection object resulted in an error, or if
// reading a message from the queue resulted in an error.
//
// ConsumeOnce will return an amqp.Delivery object if successful. You should make sure to acknowledge the message
// once it has been processed.
func (b *Client) ConsumeOnce(queue, exchange, exchangeType, routingKey string) (*amqp.Delivery, error) {
	var err error
	var conn *Connection
	if b.consumerConn == nil {
		if conn, err = NewConnection(b.url); err != nil {
			return nil, err
		}
		b.consumerConn = conn
	}
	return b.consumerConn.ConsumeOnce(queue, exchange, exchangeType, routingKey)
}

// Publish publishes a message to an exchange using the provided routingKey.
//
// Publish will return an error if creating the underlying Connection object resulted in an error, or if
// publishing the message returned an error.
//
// Note the Publish does not guarantee the message was received by the AMQP broker. AMQP connection errors
// will most likely be picked up and indicate the message was not published. However, other potential reasons
// for delivery failures might not result in an error.
// To publish messages reliably, use PublishWithConfirm
func (b *Client) Publish(exchange, routingKey string, msg []byte) error {
	var err error
	var conn *Connection
	if b.publisherConn == nil {
		if conn, err = NewConnection(b.url); err != nil {
			return err
		}
		b.publisherConn = conn
	}
	return b.publisherConn.Publish(exchange, routingKey, msg)
}

// PublishWithConfirm publishes a message to an exchange using the provided routingKey, and waits for the AMQP
// broker to confirm the message was delivered.
//
// PublishWithConfirm will wait for the delivery confirmation for the duration passed in confirmTimeout, after which,
// the delivery will be considered as failed, and an error will be returned.
//
// PublishWithConfirm will return an error if an existing publisher object on the given exchange and routingKey
// has already been defined, without delivery confirmation support (i.e. by calling Publish, with the same arguments).
//
// PublishWithConfirm will return an error if creating the underlying Connection object resulted in an error, or if
// publishing the message returned an error, either due to an error in the underlying AMQP channel, or because
// delivery confirmation was not received after the given timeout.
func (b *Client) PublishWithConfirm(exchange, routingKey string, msg []byte, confirmTimeout time.Duration) error {
	var err error
	var conn *Connection
	if b.publisherConn == nil {
		if conn, err = NewConnection(b.url); err != nil {
			return err
		}
		b.publisherConn = conn
	}
	return b.publisherConn.PublishWithConfirm(exchange, routingKey, msg, confirmTimeout)
}

// Stop will stop the underlying consumer and publisher connections if they are defined, and disconnect
// the Client from the message broker.
func (b *Client) Stop() {
	if b.consumerConn != nil {
		b.consumerConn.Stop()
	}
	if b.publisherConn != nil {
		b.publisherConn.Stop()
	}
}
