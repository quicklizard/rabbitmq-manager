package rabbitmq_manager

import (
	"context"
	"crypto/sha256"
	"fmt"
	"github.com/streadway/amqp"
	"sync"
	"time"
)

// Connection manages an AMQP connection,
// with support for consuming and publishing messages,
// in a reliable way and handling network failure and reconnection.
type Connection struct {
	cancel      context.CancelFunc
	conn        *amqp.Connection
	consumers   map[string]*consumer
	ctx         context.Context
	lock        *sync.RWMutex
	logger      Logger
	notifyClose chan *amqp.Error
	publishers  map[string]*publisher
	running     bool
	url         string
}

// NewConnection creates a new Connection to an AMQP broker.
//
// NewConnection returns an error if connecting to the AMQP broker
// on the provided URL failed.
//
// NewConnection returns new Connection if connecting to the AMQP broker
// on the provided URL succeeded.
func NewConnection(url string) (*Connection, error) {
	ctx, cancel := context.WithCancel(context.Background())
	connection := &Connection{
		cancel:     cancel,
		consumers:  make(map[string]*consumer),
		ctx:        ctx,
		lock:       &sync.RWMutex{},
		logger:     stdLogger,
		publishers: make(map[string]*publisher),
		running:    true,
		url:        url,
	}
	conn, err := connection.getConnection()
	if err != nil {
		return nil, err
	}
	connection.conn = conn
	go connection.watchNotifyClose()
	return connection, nil
}

// SetLogger accepts any object that implements the Logger
// interface and sets it as the Connection's internal logger.
func (c *Connection) SetLogger(logger Logger) {
	c.logger = logger
}

// SetLoggerLevel sets the internal logger's log level to Level
func (c *Connection) SetLoggerLevel(l Level) {
	c.logger.SetLevel(l)
}

// Stop terminates any open AMQP channels and closes the connection to
// the AMQP broker.
func (c *Connection) Stop() {
	c.logger.Debug("closing AMQP connection")
	c.lock.Lock()
	c.running = false
	for _, cons := range c.consumers {
		cons.cancel()
		cons.channel.Close()
	}
	for _, pub := range c.publishers {
		pub.cancel()
		pub.channel.Close()
	}
	c.cancel()
	c.conn.Close()
	c.logger.Debug("AMQP connection closed")
}

// Consume creates a new consumer on a queue, bound to an exchange via a routingKey, defined with a message
// prefetch count, and a provided Handler function to handle messages from the queue.
//
// Consume returns an error if registering a consumer to the queue resulted in an error.
func (c *Connection) Consume(queue, exchange, exchangeType, routingKey string, prefetch int, handler Handler) error {
	var cons *consumer
	var err error
	if cons, err = c.getConsumer(queue, exchange, exchangeType, routingKey, prefetch, handler); err != nil {
		return err
	}
	go func(conn *Connection, cons *consumer) {
	consumerLoop:
		for {
			select {
			case msg := <-conn.delivery(cons.id):
				err := cons.handler(msg)
				if err != nil {
					c.logger.Errorf("error handling consumed message: %v", err)
				}
			case <-cons.ctx.Done():
				c.logger.Warnf("consumer [%s] shutting down", cons.id)
				break consumerLoop
			}
		}
	}(c, cons)
	return nil
}

// ConsumeOnce will consume a single message from the provided queue, bound to an exchange via a routingKey.
//
// Under the hood, ConsumeOnce will create a disposable AMQP channel, and use it to get a single message from the queue.
// Every call to ConsumeOnce creates a new AMQP channel, which may create a performance overhead if used repeatedly.
//
// ConsumeOnce will return an error if reading a message from the queue resulted in an error.
//
// ConsumeOnce will return an amqp.Delivery object if successful. You should make sure to acknowledge the message
// once it has been processed.
func (c *Connection) ConsumeOnce(queue, exchange, exchangeType, routingKey string) (*amqp.Delivery, error) {
	var err error
	var channel *amqp.Channel
	var id = c.uniqueKey(fmt.Sprintf("%s:%s:%s", queue, exchange, routingKey))
	defer c.safeChannelClose(channel)
	if channel, _, err = c.consumerChannel(id, queue, exchange, exchangeType, routingKey, 1); err != nil {
		return nil, err
	}
	msg, ok, err := channel.Get(queue, false)
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, fmt.Errorf("queue %s has no messages", queue)
	}
	return &msg, nil
}

// Publish publishes a message to an exchange using the provided routingKey.
//
// Publish will return an error if publishing the message returned an error.
//
// Note the Publish does not guarantee the message was received by the AMQP broker. AMQP connection errors
// will most likely be picked up and indicate the message was not published. However, other potential reasons
// for delivery failures might not result in an error.
// To publish messages reliably, use PublishWithConfirm
func (c *Connection) Publish(exchange, routingKey string, msg []byte) error {
	if !c.running {
		return fmt.Errorf("connection is not available. cannot publish")
	}
	var pub *publisher
	var err error
	pub, err = c.getPublisher(exchange, routingKey, false)
	if err != nil {
		return err
	}
	c.lock.RLock()
	defer c.lock.RUnlock()
	err = pub.channel.Publish(exchange, routingKey, false, false, amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Body:         msg,
	})
	return err
}

// PublishWithConfirm publishes a message to an exchange using the provided routingKey, and waits for the AMQP
// broker to confirm the message was delivered.
//
// PublishWithConfirm will wait for the delivery confirmation for the duration passed in confirmTimeout, after which,
// the delivery will be considered as failed, and an error will be returned.
//
// PublishWithConfirm will return an error if an existing publisher object on the given exchange and routingKey
// has already been defined, without delivery confirmation support (i.e. by calling Publish, with the same arguments).
//
// PublishWithConfirm will return an error if publishing the message returned an error, either due to an error
// in the underlying AMQP channel, or because delivery confirmation was not received after the given timeout.
func (c *Connection) PublishWithConfirm(exchange, routingKey string, msg []byte, confirmTimeout time.Duration) error {
	if !c.running {
		return fmt.Errorf("connection is not available. cannot publish")
	}
	var pub *publisher
	var err error
	pub, err = c.getPublisher(exchange, routingKey, true)
	if err != nil {
		return err
	}
	if !pub.confirmable {
		return fmt.Errorf("publisher doesn't support delivery confirmation")
	}
	c.lock.RLock()
	defer c.lock.RUnlock()
	for {
		err = pub.channel.Publish(exchange, routingKey, false, false, amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			Body:         msg,
		})
		if err != nil {
			return err
		}
		select {
		case confirm := <-pub.notifyConfirm:
			if confirm.Ack {
				return nil
			}
		case <-time.After(confirmTimeout):
			return fmt.Errorf("server failed to acknowledge message reciept within %s", confirmTimeout.String())
		case <-pub.ctx.Done():
			return fmt.Errorf("publisher stopped before message reciept confirmed")
		}
	}
	return nil
}

// watchNotifyClose runs as a goroutine and watches for messages on the Connection's
// notifyClose channel.
// When a notifyClose message is received, restoring the underlying AMQP connection
// will be attempted, and if successful, any registered consumers and publishers will
// receive a new AMQP channel to use.
func (c *Connection) watchNotifyClose() {
watchLoop:
	for {
		select {
		case <-c.notifyClose:
			c.logger.Warn("connection closed")
			if !c.running {
				break watchLoop
			}
			c.lock.RLock()
			c.logger.Warn("reconnecting to broker")
			c.reconnect()
			c.logger.Warn("refreshing existing channels")
			c.refreshChannels()
			c.lock.RUnlock()
			c.logger.Warn("connection restored")
		case <-c.ctx.Done():
			break watchLoop
		}
	}
}

// reconnect tries to re-establish a new connection to the AMQP broker.
func (c *Connection) reconnect() {
reconnectLoop:
	for {
		var conn *amqp.Connection
		var err error
		if !c.running {
			break reconnectLoop
		} else {
			conn, err = c.getConnection()
			if err != nil {
				time.Sleep(1 * time.Second)
			} else {
				c.conn = conn
				break reconnectLoop
			}
		}
	}
}

// getConsumer creates and returns a consumer object on a given queue, bound to an exchange via a routingKey.
// Consumers are uniquely identified by the queue, exchange and routingKey they use to consume messages.
// Therefore, no two consumers can consume from the same queue using the same binding options.
//
// getConsumer will return an error if a consumer with the queue name and binding options has already been defined, or if
// creating the consumer's underlying AMQP channel resulted in an error.
func (c *Connection) getConsumer(queue, exchange, exchangeType, routingKey string, prefetch int, handler Handler) (*consumer, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	var cons *consumer
	var id = c.uniqueKey(fmt.Sprintf("%s:%s:%s", queue, exchange, routingKey))
	var found bool
	var err error
	var channel *amqp.Channel
	var deliveryChannel <-chan amqp.Delivery
	if _, found = c.consumers[id]; found {
		return nil, fmt.Errorf("consumer already exists. cannot re-initialize")
	}
	if channel, deliveryChannel, err = c.consumerChannel(id, queue, exchange, exchangeType, routingKey, prefetch); err != nil {
		return nil, err
	}
	ctx, cancel := context.WithCancel(context.Background())
	cons = &consumer{
		cancel:       cancel,
		channel:      channel,
		ctx:          ctx,
		delivery:     deliveryChannel,
		exchange:     exchange,
		exchangeType: exchangeType,
		handler:      handler,
		id:           id,
		queue:        queue,
		prefetch:     prefetch,
		routingKey:   routingKey,
	}
	c.consumers[id] = cons
	return cons, nil
}

// consumerChannel creates and returns a new AMQP channel used to consume messages from a queue, bound to an exchange via a routingKey,
// with provided prefetch size.
// When successful, consumerChannel returns an open AMQP channel and an amqp.Delivery channel, used to read messages from.
// consumerChannel returns an error if creating the underlying AMQP channel or the related queue and delivery binding failed.
func (c *Connection) consumerChannel(id, queue, exchange, exchangeType, routingKey string, prefetch int) (*amqp.Channel, <-chan amqp.Delivery, error) {
	var err error
	var channel *amqp.Channel
	var deliveryChannel <-chan amqp.Delivery
	channel, err = c.conn.Channel()
	if err != nil {
		return nil, nil, err
	}
	if err = channel.ExchangeDeclare(exchange, exchangeType, true, false, false, false, nil); err != nil {
		return nil, nil, err
	}
	if _, err = channel.QueueDeclare(queue, true, false, false, false, nil); err != nil {
		return nil, nil, err
	}
	if err = channel.QueueBind(queue, routingKey, exchange, false, nil); err != nil {
		return nil, nil, err
	}
	if err = channel.Qos(prefetch, 0, false); err != nil {
		return nil, nil, err
	}
	if deliveryChannel, err = channel.Consume(queue, id, false, false, false, false, nil); err != nil {
		return nil, nil, err
	}
	return channel, deliveryChannel, nil
}

// getPublisher returns a publisher object used to publish messages to an exchange via a routingKey.
// Publishers are uniquely identified by their exchange and routingKey.
// Calling getPublisher with a given exchange and routingKey will create and return a new publisher object.
// Subsequent calls to getPublisher with the same exchange and routingKey, will return the same publisher object.
// Once a publisher for a given exchange and routingKey was created, and defined with a given confirmable flag, it cannot be
// re-created with a different value.
//
// getPublisher will return an error if creating the underlying AMQP channel failed
func (c Connection) getPublisher(exchange, routingKey string, confirmable bool) (*publisher, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	var pub *publisher
	var id = c.uniqueKey(fmt.Sprintf("%s:%s", exchange, routingKey))
	var found bool
	var err error
	var channel *amqp.Channel
	if pub, found = c.publishers[id]; found {
		return pub, nil
	}
	if channel, err = c.publisherChannel(); err != nil {
		return nil, err
	}
	ctx, cancel := context.WithCancel(context.Background())
	pub = &publisher{
		cancel:      cancel,
		channel:     channel,
		confirmable: confirmable,
		exchange:    exchange,
		ctx:         ctx,
		id:          id,
		routingKey:  routingKey,
	}
	if confirmable {
		pub.channel.Confirm(false)
		var notifyConfirm = make(chan amqp.Confirmation)
		pub.notifyConfirm = pub.channel.NotifyPublish(notifyConfirm)
	}
	c.publishers[id] = pub
	return pub, nil
}

// publisherChannel creates and returns an AMQP channel used to publish
// messages to AMQP broker.
//
// publisherChannel will return an error if creating the AMQP channel failed.
func (c *Connection) publisherChannel() (*amqp.Channel, error) {
	var err error
	var channel *amqp.Channel
	channel, err = c.conn.Channel()
	if err != nil {
		return nil, err
	}
	return channel, nil
}

// getConnection connects to AMQP broker using the url defined when creating
// the Connection.
//
// getConnection returns an AMQP connection if successful.
// getConnection returns an error if connecting to AMQP broker failed.
func (c *Connection) getConnection() (*amqp.Connection, error) {
	notifyClose := make(chan *amqp.Error)
	conn, err := amqp.Dial(c.url)
	if err != nil {
		return nil, err
	}
	c.notifyClose = conn.NotifyClose(notifyClose)
	return conn, nil
}

// delivery returns the amqp.Delivery channel associated with the provided id.
// In case the Connection is recovering from a network failure, delivery will
// wait to acquire a lock, which becomes available only after the connection
// and any accompanying channels have been restored.
func (c *Connection) delivery(id string) <-chan amqp.Delivery {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.consumers[id].delivery
}

// refreshChannels refreshes all AMQP channels used by publisher
// and consumer objects already defined on the Connection.
func (c *Connection) refreshChannels() {
	// for publisher channels, we simply remove the channels from the map
	// calling Publish will re-create the channel dynamically
	for _, pub := range c.publishers {
		var err error
		var pubChan *amqp.Channel
		c.safeChannelClose(pub.channel)
		if pubChan, err = c.publisherChannel(); err != nil {
			c.logger.Errorf("cannot refresh publisher channel")
			continue
		}
		pub.channel = pubChan
		if pub.confirmable {
			var notifyConfirm = make(chan amqp.Confirmation)
			pub.notifyConfirm = pub.channel.NotifyPublish(notifyConfirm)
		}
	}
	// for consumers, iterate over registered consumers
	// and re-create each consumer's amqp channel and delivery
	for id, cons := range c.consumers {
		channel, deliveryChannel, err := c.consumerChannel(id, cons.queue, cons.exchange, cons.exchangeType, cons.routingKey, cons.prefetch)
		if err != nil {

		} else {
			cons.channel = channel
			cons.delivery = deliveryChannel
		}
	}
}

// uniqueKey creates a consistent hash from a given source and returns it.
func (c *Connection) uniqueKey(src string) string {
	sh := sha256.Sum256([]byte(src))
	return fmt.Sprintf("%x", sh)
}

// safeChannelClose will try to safely close an AMQP channel, if it's available.
func (c *Connection) safeChannelClose(channel *amqp.Channel) {
	if channel != nil {
		channel.Close()
	}
}
