package rabbitmq_manager

import (
	"context"
	"github.com/streadway/amqp"
)

// Handler defines a function that handles messages received by a consumer from the message broker
type Handler func(msg amqp.Delivery) error

// consumer defines a message consumer on a given queue
// bound to an exchange via a routingKey, with a message prefetch count
// and a message Handler function.
//
// consumer is used by Connection to encapsulate queue consumers, in a way that allows
// robust reconnection in case of AMQP connection failures.
type consumer struct {
	cancel       context.CancelFunc
	channel      *amqp.Channel
	delivery     <-chan amqp.Delivery
	ctx          context.Context
	exchange     string
	exchangeType string
	handler      Handler
	id           string
	queue        string
	prefetch     int
	routingKey   string
}

// publisher defines a message publisher on a given exchange
// bound to an exchange via a routingKey.
//
// publisher is used by Connection to encapsulate message publishers, in a way that allows
// robust reconnection in case of AMQP connection failures.
type publisher struct {
	cancel        context.CancelFunc
	channel       *amqp.Channel
	confirmable   bool
	ctx           context.Context
	exchange      string
	id            string
	notifyConfirm chan amqp.Confirmation
	routingKey    string
}
