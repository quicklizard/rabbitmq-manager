package rabbitmq_manager

import (
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/suite"
	"testing"
	"time"
)

type ConnectionTestSuite struct {
	conn *Connection
	suite.Suite
}

func (s *ConnectionTestSuite) SetupTest() {
	conn, err := NewConnection("amqp://quickrabbit:qu1ckr4bb1t@localhost:5672/")
	if err != nil {
		s.T().Fatal(err)
	}
	s.conn = conn
}

func (s *ConnectionTestSuite) TearDownTest() {
	s.conn.Stop()
}

func (s *ConnectionTestSuite) TestPublish() {
	var exchange = "amq.direct"
	var routingKey = "test.key"
	var queue = "test_queue"
	var body = "testing message"
	pub, err := s.conn.getPublisher(exchange, routingKey, false)
	s.Assert().NoError(err)
	_, err = pub.channel.QueueDeclare(queue, true, false, false, false, nil)
	s.Assert().NoError(err)
	err = pub.channel.QueueBind(queue, routingKey, exchange, false, nil)
	s.Assert().NoError(err)
	err = s.conn.Publish(exchange, routingKey, []byte(body))
	time.Sleep(1 * time.Second)
	s.Assert().NoError(err)
	msg, ok, err := pub.channel.Get(queue, false)
	s.Assert().NoError(err)
	s.Assert().True(ok)
	s.Assert().Equal(body, string(msg.Body))
	pub.channel.QueueDelete(queue, false, false, true)
	s.conn.logger.Infof("--- got published message: %s", string(msg.Body))
}

func (s *ConnectionTestSuite) TestPublishConfirm() {
	var exchange = "amq.direct"
	var routingKey = "test.key"
	var queue = "test_queue"
	var body = "testing message"
	pub, err := s.conn.getPublisher(exchange, routingKey, true)
	s.Assert().NoError(err)
	_, err = pub.channel.QueueDeclare(queue, true, false, false, false, nil)
	s.Assert().NoError(err)
	err = pub.channel.QueueBind(queue, routingKey, exchange, false, nil)
	s.Assert().NoError(err)
	err = s.conn.PublishWithConfirm(exchange, routingKey, []byte(body), 5*time.Second)
	s.Assert().NoError(err)
	msg, ok, err := pub.channel.Get(queue, false)
	s.Assert().NoError(err)
	s.Assert().True(ok)
	s.Assert().Equal(body, string(msg.Body))
	err = s.conn.PublishWithConfirm(exchange, routingKey, []byte(body), 1*time.Nanosecond)
	s.Assert().Error(err)
	pub.channel.QueueDelete(queue, false, false, true)
	s.conn.logger.Infof("--- got published message: %s", string(msg.Body))
}

func (s *ConnectionTestSuite) TestConsume() {
	var exchange = "amq.direct"
	var routingKey = "test.key"
	var queue = "test_queue"
	var body = "testing message"
	var prefetch = 1
	pch, _ := s.conn.publisherChannel()
	pch.QueueDeclare(queue, true, false, false, false, nil)
	pch.QueueBind(queue, routingKey, exchange, false, nil)
	err := s.conn.Publish(exchange, routingKey, []byte(body))
	s.Assert().NoError(err)
	s.conn.Consume(queue, exchange, "direct", routingKey, prefetch, func(msg amqp.Delivery) error {
		s.Assert().Equal(body, string(msg.Body))
		s.conn.logger.Infof("--- got published message: %s", string(msg.Body))
		msg.Ack(false)
		return nil
	})
	pch.QueueDelete(queue, false, false, true)
}

func (s *ConnectionTestSuite) TestReconnect() {
	var exchange = "amq.direct"
	var routingKey = "test.key"
	var queue = "test_queue"
	var body = "testing message"
	pch, _ := s.conn.publisherChannel()
	pch.QueueDeclare(queue, true, false, false, false, nil)
	pch.QueueBind(queue, routingKey, exchange, false, nil)
	s.conn.conn.Close()
	err := s.conn.Publish(exchange, routingKey, []byte(body))
	s.Assert().Error(err)
	time.Sleep(2 * time.Second)
	err = s.conn.Publish(exchange, routingKey, []byte(body))
	s.Assert().NoError(err)
	pch, _ = s.conn.publisherChannel()
	msg, ok, err := pch.Get(queue, false)
	s.Assert().NoError(err)
	s.Assert().True(ok)
	s.Assert().Equal(body, string(msg.Body))
	pch.QueueDelete(queue, false, false, true)
}

func TestConnectionSuite(t *testing.T) {
	suite.Run(t, new(ConnectionTestSuite))
}
